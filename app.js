//alert("Welcome!");
swal({
    title: "Welcome",
    text: "Good People!",
    confirmButtonText: "AwKay",
    confirmButtonColor: "#292b2c"
})
const colors = ["#008E9B", "D65D81", "#FF6F91", "#FF9671"];
let button = document.getElementById('btn-color');
let warna = document.querySelector('.warna');

//Translate
let translateButton = document.getElementById('btn-translate');
let navAbout = document.getElementById('about');
let navEdu   = document.getElementById('education');
let navExp   = document.getElementById('experiences');
let navContact = document.getElementById('contact');
let banner1 = document.getElementById('banner1');
let banner21 = document.getElementById('banner2-1');
let banner22 = document.getElementById('banner2-2');
let banner23 = document.getElementById('banner2-3');

button.addEventListener('click', function(){
    let randomNumber = Math.floor(Math.random() * colors.length);
    warna.innerHTML = colors[randomNumber];
    document.body.style.background = colors[randomNumber];
});

const nav = ["Tentang Saya", "Pendidikan", "Pengalaman", "Hubungi Saya"];
const banner1Text = "Hi, kamu bisa memanggilku Jody. Aku baru lulus dari Jurusan Teknik Komputer. Aku suka coding dan berkolaborasi dengan tim. Salam kenal!";
const banner1and2Text = ["Aku", "Manusia"];
const banner23Text = "Terkadang aku bingung, kenapa orang-orang selalu bertanya mengenai gender. Aku cuma mau bilang kalau gender itu ngga berarti apapun. Kamu tetaplah dirimu dan kamu bisa jadi apapun yang kamu mau terlepas dari gendermu. Kalau di KTP sih genderku 'Laki-laki'. Ngga apa-apa juga sih kalian tau."
translateButton.addEventListener('click', function translate(){
    navAbout.innerHTML   = nav[0];
    navEdu.innerHTML     = nav[1];
    navExp.innerHTML     = nav[2];
    navContact.innerHTML = nav[3];
    banner1.innerHTML    = banner1Text;
    banner21.innerHTML   = banner1and2Text[0];
    banner22.innerHTML   = banner1and2Text[1];
    banner23.innerHTML   = banner23Text;
    
});